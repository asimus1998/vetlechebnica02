'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('home')
Route.on('/about').render('about')
Route.on('/about/history').render('history')
Route.on('/about/workers').render('workers')
Route.on('/about/sections').render('sections')

Route.on('/services').render('services/index')
Route.on('/services/desinfektion').render('services/desinfektion')
Route.on('/services/apteka').render('services/apteka')
