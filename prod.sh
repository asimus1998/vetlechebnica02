docker rmi $(docker images --filter "dangling=true" -q --no-trunc)

docker build -t vetlechebnica-web -f Dockerfile .;

docker rm -f "vetlechebnica-web";

docker run \
    --name "vetlechebnica-web" \
    -e TZ="Europe/Moscow" \
    -p 8084:8084 \
    -v $(pwd):"/app" \
    -v /app/node_modules \
    -d \
    vetlechebnica-web;
